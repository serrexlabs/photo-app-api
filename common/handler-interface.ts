export interface IHandler {
  handle (...args: any): Promise<any>
}

import { Container } from 'inversify'
import { GetPhotoGridQueryHandler } from '../src/photo-grid/query/get-photo-grid-query-handler'
import { IPhotoGridRepository } from '../src/photo-grid/repository/photo-grid-repository-interface'
import { PhotoGridRepository } from '../src/photo-grid/repository/photo-grid-repository'
import { CreatePhotoGridCommandHandler } from '../src/photo-grid/command/create-photo-grid-command-handler'

export const container = new Container()
container.bind<GetPhotoGridQueryHandler>(GetPhotoGridQueryHandler).toSelf()
container.bind<CreatePhotoGridCommandHandler>(CreatePhotoGridCommandHandler).toSelf()
container.bind<IPhotoGridRepository>('IPhotoGridRepository').to(PhotoGridRepository)

import {
    GraphQLObjectType,
    GraphQLID
} from 'graphql'

import photoGrid from '../src/photo-grid/graphql/'

export const mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    photoGrid: {
      type: photoGrid.types.GraphQLPhotoGridMutations,
      description: 'Photo Grid mutation',
      args: {
        id: {
          description: 'id of the photo grid',
          type: GraphQLID
        }
      },
      resolve: photoGrid.resolvers.photoGridMutation
    }
  }
})

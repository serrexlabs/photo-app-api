import {
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLList,
    GraphQLID
} from 'graphql'

import photoGrid from '../src/photo-grid/graphql/'

export const query = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    photoGrid: {
      type: GraphQLList(photoGrid.types.GraphQLPhotoGrid),
      args: {
        ids: {
          type: GraphQLList(GraphQLID)
        }
      },
      resolve: photoGrid.resolvers.photoGrid
    }
  })
})

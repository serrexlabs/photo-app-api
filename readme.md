# Photo App API

Technologies/Paradigms
* GraphQL
* CQRS
* DI (Dependency Injections)
* Design Patterns

### Mutation

install dependancies
```sh
mutation {
  photoGrid {
    add([ {id: 123, url: "https://ex.com/img.png"}]) {
      id,
      url
    },
    update([ {id: 123, url: "https://ex.com/img.png"}]) {
      id,
      url
    }
  }
}

```

### Query

install dependancies
```sh
{
  photoGrid {
    id,
    url
  }
}

```

### Installation

install dependancies
```sh
$ yarn global add typescript
$ yarn install 
```

configure env properties 

```sh
$ cp .env.example .env
```
then replace following with acctual values 

```
DB_HOST='' #Mongodb connection string
```

### How to run 

Via Docker
```sh
$ docker-compose up -d
```
Run as in dev env
```sh
$ yarn dev
```
Build and run as prod
```sh
$ yarn prod
```

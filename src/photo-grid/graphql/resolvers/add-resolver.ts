import { container } from '../../../../config/container'
import { GetPhotoGridQueryHandler } from '../../query/get-photo-grid-query-handler'
import { CreatePhotoGridCommandHandler } from '../../command/create-photo-grid-command-handler'

export default async function resolver (source: any, args: any) {
  const createPhotoGridCommandHandler = container.get(CreatePhotoGridCommandHandler)
  const getPhotoGridQueryHandler = container.get(GetPhotoGridQueryHandler)
  const insertedId = await createPhotoGridCommandHandler.handle(args.photos)
  const [ photoGrid ] = await getPhotoGridQueryHandler.handle([insertedId])

  return photoGrid
}

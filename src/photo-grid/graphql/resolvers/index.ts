import photoGridResolver from './photo-grid-resolver'
import photoGridMutationResolver from './photo-grid-mutation-resolver'
import photoGridAddMutationResolver from './add-resolver'

export default {
  photoGrid: photoGridResolver,
  photoGridAddMutation: photoGridAddMutationResolver,
  photoGridMutation: photoGridMutationResolver
}

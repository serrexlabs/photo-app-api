import { container } from '../../../../config/container'
import { GetPhotoGridQueryHandler } from '../../query/get-photo-grid-query-handler'

export default async function resolver (source: any, args: { ids: string[]}) {
  const getPhotoGridQueryHandler = container.get(GetPhotoGridQueryHandler)
  return getPhotoGridQueryHandler.handle(args.ids || [])
}

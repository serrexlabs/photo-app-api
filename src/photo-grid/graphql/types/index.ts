import { GraphQLPhotoGrid } from './photo-grid-type'
import { GraphQLPhotoGridMutations } from './photo-grid-mutation-type'
import { GraphQLPhotoInput } from './photo-type-input'
import { GraphQLPhoto } from './photo-type'

export default {
  GraphQLPhotoGrid,
  GraphQLPhotoGridMutations,
  GraphQLPhotoInput,
  GraphQLPhoto
}

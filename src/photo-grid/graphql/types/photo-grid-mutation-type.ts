import {
  GraphQLObjectType,
  GraphQLList,
  GraphQLString
} from 'graphql'
import { GraphQLPhotoGrid } from './photo-grid-type'

import resolvers from '../resolvers'
import { GraphQLPhotoInput } from './photo-type-input'

export const GraphQLPhotoGridMutations = new GraphQLObjectType({
  name: 'GraphQLPhotoGridMutations',
  fields: {
    add: {
      resolve: resolvers.photoGridAddMutation,
      type: GraphQLPhotoGrid,
      description: 'Add new photo grid',
      args: {
        photos: {
          type: GraphQLList(GraphQLPhotoInput),
          description: 'photos'
        }
      }
    }
  }
})

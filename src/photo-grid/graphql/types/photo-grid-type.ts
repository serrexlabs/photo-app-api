import {
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLID,
    GraphQLList
} from 'graphql'
import { GraphQLPhoto } from './photo-type'

export const GraphQLPhotoGrid = new GraphQLObjectType({
  name: 'PhotoGrid',
  description: 'Photo grid info',

  fields: {
    id: {
      type: GraphQLNonNull(GraphQLID),
      description: ' Id'
    },

    photos: {
      type: GraphQLList(GraphQLPhoto),
      description: 'Photos'
    }
  }
})

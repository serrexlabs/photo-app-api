import {
    GraphQLInputObjectType,
    GraphQLNonNull,
    GraphQLID,
    GraphQLString
} from 'graphql'

export const GraphQLPhotoInput = new GraphQLInputObjectType({
  name: 'GraphQLPhotoInput',
  description: 'Photo input',

  fields: {
    id: {
      type: GraphQLNonNull(GraphQLID),
      description: ' Id'
    },

    url: {
      type: GraphQLNonNull(GraphQLString),
      description: 'url'
    }
  }
})

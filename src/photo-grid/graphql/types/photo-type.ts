import {
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLID,
    GraphQLString
} from 'graphql'

export const GraphQLPhoto = new GraphQLObjectType({
  name: 'GraphQLPhoto',
  description: 'Photo info',

  fields: {
    id: {
      type: GraphQLNonNull(GraphQLID),
      description: ' Id'
    },

    url: {
      type: GraphQLNonNull(GraphQLString),
      description: 'url'
    }
  }
})

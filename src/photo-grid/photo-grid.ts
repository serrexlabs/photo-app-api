import * as mongoose from 'mongoose'

export interface IPhotoGrid {
  id?: string,
  photos: Array<{ id: number, url: string}>,
}

export const PhotoGridSchema = new mongoose.Schema({
  photos: { type: Array, required: true }
})

const PhotoGrid = mongoose.model<IPhotoGrid & mongoose.Document>('PhotoGrid', PhotoGridSchema)
export { PhotoGrid }

import 'reflect-metadata'
import { inject, injectable } from 'inversify'
import { IPhotoGridRepository } from '../repository/photo-grid-repository-interface'
import { IHandler } from '../../../common/handler-interface'

@injectable()
export class GetPhotoGridQueryHandler implements IHandler {
  private photoGridRepository: IPhotoGridRepository

  constructor (@inject('IPhotoGridRepository') photoGridRepository: IPhotoGridRepository) {
    this.photoGridRepository = photoGridRepository
  }

  public async handle (ids: string[]): Promise<any> {
    return this.photoGridRepository.findByIds(ids)
  }

}

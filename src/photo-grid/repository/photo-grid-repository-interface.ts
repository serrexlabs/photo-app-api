import { IPhotoGrid } from '../photo-grid'

export interface IPhotoGridRepository {
  findByIds (ids: string[]): Promise<IPhotoGrid[] | []>
  create (photos: Array<{id: number, url: string}>): Promise<string>
}

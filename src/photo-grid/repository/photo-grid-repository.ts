import { IPhotoGridRepository } from './photo-grid-repository-interface'
import { IPhotoGrid, PhotoGrid } from '../photo-grid'
import { ObjectID } from 'mongodb'
import { injectable } from 'inversify'

@injectable()
export class PhotoGridRepository implements IPhotoGridRepository {

  public async findByIds (ids: string[]): Promise<IPhotoGrid[] | []> {

    if (ids.length === 0) {
      return PhotoGrid.find()
    }

    return PhotoGrid.find({
      _id: { $in: ids.map((id) => new ObjectID(id)) }
    })
  }

  public async create (photos: Array<{id: number, url: string}>): Promise<string> {
    const [ doc ] = await PhotoGrid.create([ { photos }])
    return doc._id
  }

}
